<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en" ng-app="App">
<head>
	<meta charset="utf-8">
	<title>CRUD CODIGNITER e angularJS</title>
	<link rel="stylesheet" href="<?=  config_item('base_url') ?>application\libraries\css\bootstrap.min.css">
	<script src="<?=  config_item('base_url') ?>application\libraries\js\jquery.js"></script>
	<script src="<?=  config_item('base_url') ?>application\libraries\js\bootstrap.min.js"></script>
	<script src="<?=  config_item('base_url') ?>application\libraries\js\angular.min.js"></script>
	<script src="<?=  config_item('base_url') ?>application\libraries\js\jqueryMask.min.js"></script>
	<script src="<?=  config_item('base_url') ?>application\views\js\welcome_message.js"></script>

<style type="text/css">
	input,button{
		margin-top:5px;
		margin-bottom:5px;
	}
	.verDetalhes{
		text-align:center;
		cursor:pointer;
	}
	.esconder{
		display:none;
	}
</style>

</head>
<body>

<div id="container" class="container" ng-controller="indexCntrl">

	<div class="row">
		Cadastre o aluno:<br><br>
	</div>	

	<div class="row form-cadastro-aluno">
		<div class="col-sm-6">
			<p>Dados pessoais: </p>
			<input type="text" ng-model="nome_completo" placeholder="nome completo" class="col-sm-12">
			<input type="text" ng-model="email" placeholder="email" class="col-sm-12">
			<input type="password" ng-model="senha" placeholder="senha" class="col-sm-12">
			<select ng-model='curso' ng-init="curso='default'" class="col-sm-12">
				<option value='default'>Selecione seu curso!</option>
				<option ng-repeat='atb_curso in listaCursos' value='{{atb_curso.id_curso}}'>{{atb_curso.nome_curso}}</option>
			</select>
			<input type="text" ng-model="telefone" placeholder="telefone" class="col-sm-12 telefone_mask">
			<select ng-model='estado' ng-init="estado='default'" class="col-sm-12">
				<option value='default'>Selecione seu estado</option>
				<option value="AC">Acre</option>
				<option value="AL">Alagoas</option>
				<option value="AP">Amapá</option>
				<option value="AM">Amazonas</option>
				<option value="BA">Bahia</option>
				<option value="CE">Ceará</option>
				<option value="DF">Distrito Federal</option>
				<option value="ES">Espírito Santo</option>
				<option value="GO">Goiás</option>
				<option value="MA">Maranhão</option>
				<option value="MT">Mato Grosso</option>
				<option value="MS">Mato Grosso do Sul</option>
				<option value="MG">Minas Gerais</option>
				<option value="PA">Pará</option>
				<option value="PB">Paraíba</option>
				<option value="PR">Paraná</option>
				<option value="PE">Pernambuco</option>
				<option value="PI">Piauí</option>
				<option value="RJ">Rio de Janeiro</option>
				<option value="RN">Rio Grande do Norte</option>
				<option value="RS">Rio Grande do Sul</option>
				<option value="RO">Rondônia</option>
				<option value="RR">Roraima</option>
				<option value="SC">Santa Catarina</option>
				<option value="SP">São Paulo</option>
				<option value="SE">Sergipe</option>
				<option value="TO">Tocantins</option>
			</select>
			<input type="text" ng-model="cidade" placeholder="cidade" class="col-sm-12">
		</div>

		<div class="col-sm-6">
			<p>Dados da cobrança:</p>
			<input type="text" ng-model="nome_completo_responsavel" placeholder="nome completo responsavel"  class="col-sm-12">
			<input type="text" ng-model="telefone_responsavel" placeholder="telefone responsavel" class="col-sm-12 telefone_mask">
			<select ng-model='estado_responsavel' ng-init="estado_responsavel='default'" class="col-sm-12">
				<option value='default'>Selecione estado responsavel</option>
				<option value="AC">Acre</option>
				<option value="AL">Alagoas</option>
				<option value="AP">Amapá</option>
				<option value="AM">Amazonas</option>
				<option value="BA">Bahia</option>
				<option value="CE">Ceará</option>
				<option value="DF">Distrito Federal</option>
				<option value="ES">Espírito Santo</option>
				<option value="GO">Goiás</option>
				<option value="MA">Maranhão</option>
				<option value="MT">Mato Grosso</option>
				<option value="MS">Mato Grosso do Sul</option>
				<option value="MG">Minas Gerais</option>
				<option value="PA">Pará</option>
				<option value="PB">Paraíba</option>
				<option value="PR">Paraná</option>
				<option value="PE">Pernambuco</option>
				<option value="PI">Piauí</option>
				<option value="RJ">Rio de Janeiro</option>
				<option value="RN">Rio Grande do Norte</option>
				<option value="RS">Rio Grande do Sul</option>
				<option value="RO">Rondônia</option>
				<option value="RR">Roraima</option>
				<option value="SC">Santa Catarina</option>
				<option value="SP">São Paulo</option>
				<option value="SE">Sergipe</option>
				<option value="TO">Tocantins</option>
			</select>
			<input type="text" ng-model="cidade_responsavel" placeholder="cidade responsavel" class="col-sm-12">
		</div>	

		<div class="col-sm-12">
			<button type="button" class="btn btn-success cadastrarAluno" ng-click="cadastrarAluno()">Cadastrar aluno</button>
		</div>

	</div>

	<div class="row">
		<br><br><br>Lista de alunos:<br><br><br>
	</div>	

	<div class="row">

		<table class="table">
	    <thead>
	      <tr>
	        <th>nome</th>
	        <th>Email</th>
	        <th>Telefone</th>
	        <th>Operações</th>
	      </tr>
	    </thead>
	    <tbody>
	      <tr>
	    	<td><input type="text" ng-model="search_nome"     placeholder="Pesquise por nome"></td>		
	    	<td><input type="text" ng-model="search_email"    placeholder="Pesquise por email"></td>		
	    	<td><input type="text" ng-model="search_telefone" placeholder="Pesquise por telefone" class="telefone_mask"></td>
	    	<td></td>		
	      </tr>

	      <tr ng-repeat="atb_aluno in listaAluno | filter:{'nome' : search_nome,'email' : search_email, 'telefone' : search_telefone}">
	        <td>{{atb_aluno.nome}}</td>
	        <td>{{atb_aluno.email}}</td>
	        <td>{{atb_aluno.telefone}}</td>
	        <td>
	        	<span ng-click="exibirDetalhes(atb_aluno.id)" class="verDetalhes">[ver detalhes]</span>
	        	<span ng-click="inativarAluno(atb_aluno.id)" class="verDetalhes">[inativar]</span>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	</div>


	<div class="row esconder detalheAluno">
		<div class="col-sm-12">
			<br><br><br>
			<div class="col-sm-11">
				DETALHES DO ALUNO:
			</div>
			<div class="col-sm-1">
				<i class="fa fa-window-close fecharDetalhes verDetalhes" aria-hidden="true">X</i>
			</div>
			<br><br><br>
		</div>
		<table class="table">
			<tbody>
				<tr>
					<td>nome completo</td>
					<td>{{dt_nome}}</td>
				</tr>
				<tr>
					<td>email</td>
					<td>{{dt_email}}</td>
				</tr>
				<tr>
					<td>Curso</td>
					<td>{{dt_nome_curso}}</td>
				</tr>
				<tr>
					<td>telefone</td>
					<td>{{dt_telefone}}</td>
				</tr>
				<tr>
					<td>estado</td>
					<td>{{dt_estado}}</td>
				</tr>
				<tr>
					<td>cidade</td>
					<td>{{dt_cidade}}</td>
				</tr>
				<tr>
					<td>Nome completo responsavel</td>
					<td>{{dt_nome_responsavel}}</td>
				</tr>
				<tr>
					<td>telefone responsavel</td>
					<td>{{dt_telefone_responsavel}}</td>
				</tr>
				<tr>
					<td>estado responsavel</td>
					<td>{{dt_estado_responsavel}}</td>
				</tr>
				<tr>
					<td>cidade responsavel</td>
					<td>{{dt_cidade_responsavel}}</td>
				</tr>
			</tbody>
		</table>

	</div>

</div>
</body>
</html>