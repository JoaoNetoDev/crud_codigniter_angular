	
	$(document).ready(function(){
		$('.telefone_mask').mask('(00) 0000-0000');

		$('.fecharDetalhes').click(function(){
			$('.detalheAluno').css('display','none');
		});

	});

	var app = angular.module('App', []);

	app.controller('indexCntrl', function($scope,$http) {

		//LISTA DE ALUNOS
		function listarAlunos(){
			$http.get("index.php/welcome/listarAlunos/").then(function(data) {
	        	$scope.listaAluno = data.data;
	   		});
		}

		$scope.cadastrarAluno = function(){
			
			var jsonDados = {
				'nome_completo': $scope.nome_completo,
				'email': $scope.email,
				'senha': $scope.senha,
				'curso': $scope.curso,
				'telefone': $scope.telefone,
				'estado': $scope.estado,
				'cidade': $scope.cidade,
				'nome_completo_responsavel': $scope.nome_completo_responsavel,
				'telefone_responsavel': $scope.telefone_responsavel,
				'estado_responsavel': $scope.estado_responsavel,
				'cidade_responsavel': $scope.cidade_responsavel
			};

			// modificar para a variavel BASEURL
			$http.post("index.php/welcome/cadastrar/",jsonDados).then(function(data) {
				$('.cadastrarAluno').val('Cadastrando ...');
				console.log(data.data);
				if(data.data == true || data.data == 'true'){
					alert('cadastrado com sucesso!');
				}else{
					alert('erro ao cadastrar :(');
				}
				listarAlunos();
			});

			
			$('.cadastrarAluno').val('Cadastrar aluno');

		}

		$scope.inativarAluno = function(id_aluno){
			var jsonDados = {'id_aluno':id_aluno};
			$http.post("index.php/welcome/inativarAluno/",jsonDados).then(function(data){
				alert('inativado com sucesso!');
				listarAlunos();
			});
		}

		$scope.exibirDetalhes = function (id_aluno){
			$(".detalheAluno").css("display","none");

			$http.get("index.php/welcome/buscarAluno/"+id_aluno).then(function(data) {
	        	var meusDados = data.data[0];

	        	$scope.dt_cidade = meusDados.cidade;
	        	$scope.dt_cidade_responsavel = meusDados.cidade_responsavel;
	        	$scope.dt_email = meusDados.email;
	        	$scope.dt_estado = meusDados.estado;
	        	$scope.dt_estado_responsavel = meusDados.estado_responsavel;
	        	$scope.dt_nome = meusDados.nome;
	        	$scope.dt_nome_curso = meusDados.nome_curso;
	        	$scope.dt_nome_responsavel = meusDados.nome_responsavel;
	        	$scope.dt_telefone = meusDados.telefone;
	        	$scope.dt_telefone_responsavel = meusDados.telefone_responsavel;

	   		});

			$(".detalheAluno").css("display","block");
		}

		$http.get("index.php/welcome/listarCursos/").then(function(data) {
        	$scope.listaCursos = data.data;
   		});


		listarAlunos();

	});	
