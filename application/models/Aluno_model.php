<?php
class Aluno_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function cadastrar($dados)
		{
	        $data = array(
			        'id' 		  => null,
			        'ativo' 	  => 1,
			        'nome' 		  => $dados['nome_completo'],
			        'email' 	  => $dados['email'],
			        'senha' 	  => sha1($dados['senha']),
			        'fk_id_curso' => $dados['curso'],
			        'telefone' 	  => $dados['telefone'],
			        'estado' 	  => $dados['estado'],
			        'cidade' 	  => $dados['cidade'],
			        'nome_responsavel' 		=> $dados['nome_completo_responsavel'],
			        'telefone_responsavel' 	=> $dados['telefone_responsavel'],
			        'estado_responsavel' 	=> $dados['estado_responsavel'],
			        'cidade_responsavel' 	=> $dados['cidade_responsavel']
			);

			$retorno = $this->db->insert('aluno', $data);
			return $retorno;
		}

		public function listarAlunos()
		{
			$retorno = $this->db->query("SELECT * FROM aluno WHERE ativo = 1");
            return $retorno->result_array();
		}

		public function detalheAluno($id)
		{
			$sql = "SELECT 
						* 
					FROM aluno 
					JOIN curso ON aluno.fk_id_curso = curso.id_curso 
					WHERE id = ?
					";
			$retorno = $this->db->query($sql, array( (int)$id ));
			return $retorno->result_array();
		}
        
		public function inativarAluno($id)
		{
                $this->ativo = 0;
                return $this->db->update('aluno', $this, array('id' => $id));			
		}

}

?>