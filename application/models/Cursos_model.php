<?php
class Cursos_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

		public function listaCursos()
		{
                $query = $this->db->get('curso');
                return $query->result_array();
		}
        
}

?>